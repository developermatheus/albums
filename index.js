// Importar uma biblioteca pra poder usar
import React from 'react';
import { AppRegistry, View } from 'react-native';
import Header from './src/components/header';
import AlbumList from './src/components/AlbumList';

// Criar um componente
const App = () => (
        <View style={{ flex: 1 }}>
            <Header tituloHeader={'Albums Props'} />
            <AlbumList />
        </View>
    );

// Renderizar o componente no aparelho
AppRegistry.registerComponent('albums', () => App);

// Somente no componente raiz utilizamos o AppRegistry.

/* Anotações:

1- Component Nesting é o ato de pegar um componente e coloca-lo dentro de outro.

2- Functional component:
const App = () => {

    return (
        <View>
            <Header tituloHeader={'Albums Props'} />
        </View>
    );
};

Este tipo de componente é mais simples de ser criado e só presta pra exibir elementos estáticos.
Ou seja, não pode sofrer alterações por manipulação de requisições http, por exemplo.

3- Class based components:

class AlbumList extends Component {
    render() {
        return (
            <View>
                <Text>Lista de Albums</Text>
            </View>
        );
    }
}

Ao contrário do componente anterior, com este é possível
lidar com qualquer data que possa ser mudado
(Pegar dados de requisições http, eventos de usuário, etc)

*/
