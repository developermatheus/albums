import React from 'react';
import { View } from 'react-native';

const Card = (props) => (
        <View style={styles.containerStyle}>
            {props.children}
        </View>
    );

const styles = {
    containerStyle: {
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        backgroundColor: '#f1f1f1',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.2, //for iOS
        elevation: 10, //for Android
        shadowRadius: 2,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 10
    }
};

export default Card;
